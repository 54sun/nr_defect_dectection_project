import time
import configparser
import ml.slice as slice
import ml.prediction as prediction
import post_processing.post_process as post_process
from os.path import join


# Record time
tic = time.perf_counter()

# Load config
config = configparser.ConfigParser()
config.read("config.ini")
bridge_image_path = config.get("Paths", "bridge_image_path")
sliced_image_folder = config.get("Paths", "sliced_image_folder")
image_info_filename = config.get("Paths", "image_info_filename")
prediction_folder = config.get("Paths", "prediction_folder")
model_path = config.get("Paths", "model_path")
model_label_path = config.get("Paths", "model_label_path")

# Run image slicing
print("\nStep 1\n[Slicing] started...")

slice.slice_image(bridge_image_path, sliced_image_folder, image_info_filename)

print("[Slicing] finished.")

# Run prediction
print("\nStep 2\n[Prediction] started...")

prediction.defect_predict(
    bridge_image_path, sliced_image_folder, prediction_folder, model_path, model_label_path)

print("[Prediction] finished.")

# Run post processing
print("\nStep 3\n[Post Processing] started...")

processor = post_process.PostProcessor(
    join(bridge_image_path, image_info_filename))

# Post processing
processor.process(join(bridge_image_path, prediction_folder))

# Calculate the coordinates for raw defects
processor.add_coord_to_raw_data(join(bridge_image_path, prediction_folder))

print("[Post Processing] finished.")

# Display time used in second
toc = time.perf_counter()
print("\nJob completed in " + f"{toc - tic:0.4f} seconds.")
