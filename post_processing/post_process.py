import pandas as pd
import fnmatch
import os


class DefectCluster:
    def __init__(self, type_id: int, included_cells, prob: float, cell_size=(224, 224), img_res: tuple[int, int] = (0, 0)) -> None:
        self.type = type_id
        self.cells = included_cells
        self.probability = prob
        self.cell_size = cell_size
        self.offset = (int((img_res[0] % cell_size[0])/2),
                       int((img_res[1] % cell_size[1])/2))

    def get_corner_cells(self):
        '''Order: top_left, top_right, bottom_left, bottom_right
        '''
        x_min = min(self.cells, key=lambda t: t[0])[0]
        y_min = min(self.cells, key=lambda t: t[1])[1]
        x_max = max(self.cells, key=lambda t: t[0])[0]
        y_max = max(self.cells, key=lambda t: t[1])[1]

        return [(x_min, y_min), (x_min, y_max), (x_max, y_min), (x_max, y_max)]

    def get_region_coordinates(self):
        '''Order: top_left, top_right, bottom_right, bottom_left

        [Drawing order of Virtual Tour]
        '''
        tl, tr, bl, br = self.get_corner_cells()

        top_left = (tl[1] * self.cell_size[0],
                    tl[0] * self.cell_size[1])
        top_right = ((tr[1]+1) * self.cell_size[0],
                     tr[0] * self.cell_size[1])
        bot_left = (bl[1] * self.cell_size[0],
                    (bl[0]+1)*self.cell_size[1])
        bot_right = ((br[1]+1) * self.cell_size[0],
                     (br[0]+1) * self.cell_size[1])

        return [self.__apply_offset(top_left), self.__apply_offset(top_right),
                self.__apply_offset(bot_right), self.__apply_offset(bot_left), ]

    def __apply_offset(self, cell):
        return ((cell[0] + self.offset[0]), (cell[1] + self.offset[1]))


class PostProcessor:
    def __init__(self, image_info_path, shape=(45, 90)) -> None:
        '''Modify the "defect filter" setting in the constructor for now.

        Parameters:
        shape (tuple[int,int]): The marix size of the breakdwon image.
        image_info_path (str): File path for the image info csv.
        '''
        self.unchecked_cells = []
        self.clusters = []
        self.defect_filter = {  # Modify as needed
            "Environment": 100,
            "No Defect Wall": 100,
            "Joint Defect": 90,
            "Crack": 90,
            "Loss of Section": 95,
            "Vegetation": 95,
            "Spalling": 99}
        self.shape = shape
        self.defects_raw: pd.DataFrame = None
        self.defects_filtered = None
        self.offset_config = self.__load_offset_config(image_info_path)
        self.cell_number_col = "Sub_image ID"
        self.predicted_defect_col = "Predicted Defect"

    def load_defects(self, filename) -> None:
        '''Load data from csv.

        No need to call seperately if using process().
        '''
        data = pd.read_csv(filename)

        if data.shape[0] != (self.shape[0]*self.shape[1]):
            print("[ERROR]"+filename)
            exit

        self.defects_raw = data

    def filter_defects(self):
        '''Apply defect filter (data will be saved in a seperate property: defects_filtered.

        No need to call seperately if using process().
        '''
        defects_by_type = []
        for defect, prob in self.defect_filter.items():
            df = self.defects_raw[(self.defects_raw[self.predicted_defect_col] == defect) & (
                self.defects_raw[defect] > prob)]
            defects_by_type.append(df)
        self.defects_filtered = pd.concat(defects_by_type)

    def convert_id_to_position(self, id) -> tuple[int, int]:
        '''Return the corresponding position (row, col).'''
        row = (id-1) // self.shape[1]
        col = (id-1) % self.shape[1]
        return (row, col)

    def convert_position_to_id(self, pos) -> int:
        '''Return the corresponding id (starting from 1).'''
        return self.shape[1]*pos[0] + pos[1] + 1

    def get_defect_cluster_cells(self, defect_type) -> list[set[tuple[int, int]]]:
        '''Return the clusters for that defect type.

        Each cluster is a set consists of defect positions only.

        A cluster is formed by including all adjacent cells with the same defect type.
        The adjacent cells is defined by get_adjacent_cells().

        No need to call seperately if using process().
        '''
        clusters = []
        df = self.defects_filtered[self.defects_filtered[self.predicted_defect_col] == defect_type].copy(
        )

        # return the empty cluster if no defects were found
        if df.shape[0] == 0:
            return clusters

        df["in cluster"] = False
        df["pos"] = df.apply(lambda row: self.convert_id_to_position(
            row[self.cell_number_col]), axis=1)

        # for each id, find its cluster, if it's not in one yet
        for index, row in df.iterrows():
            if not df.loc[index, "in cluster"]:  # don't use iterows as df updated
                cluster = {row["pos"]}
                while (len(cluster) < self.__extend_cluster(cluster, df)):
                    # The length of cluster might change after calling the method.
                    # The loop will quit when no more cells can be added.
                    pass
                clusters.append(cluster)

        return clusters

    def __extend_cluster(self, cluster: set, defects: pd.DataFrame) -> set:
        '''Return the length of the modified cluster.

        Modify the cluster by adding adjenct defect cells.
        '''
        extra = set()
        for pos in cluster:
            # find its adjacent cells
            adj_cells = self.get_adjacent_cells(pos, self.shape)
            # add the defect cells to the cluster
            for index, row in defects.loc[defects["pos"].isin(adj_cells)].iterrows():
                if not row["in cluster"]:
                    extra.add(row["pos"])
            # mark those cells as in cluster
                defects.loc[defects["pos"].isin(
                    adj_cells), "in cluster"] = True

        cluster.update(extra)
        return len(cluster)

    def get_adjacent_cells(self, postion: tuple[int, int], shape:  tuple[int, int]) -> list[tuple[int, int]]:
        '''Return the left, top, bottom, and right cells.
        '''
        adj_cells = []
        # left
        if(postion[1] > 0):
            adj_cells.append((postion[0], postion[1] - 1))

        # top
        if(postion[0] > 0):
            adj_cells.append((postion[0] - 1, postion[1]))

        # bottom
        if(postion[0] < (shape[0]-1)):
            adj_cells.append((postion[0] + 1, postion[1]))

        # right
        if(postion[1] < (shape[1]-1)):
            adj_cells.append((postion[0], postion[1] + 1))

        return adj_cells

    def cal_defect_cluster(self, img_res) -> list[DefectCluster]:
        """Return a list of clusters for each defect type.

        No need to call seperately if using process().
        """
        clusters = []
        for key in self.defect_filter.keys():
            clusters_by_type = self.get_defect_cluster_cells(key)
            for cluster in clusters_by_type:
                clusters.append(DefectCluster(
                    self.__get_defect_id(key), cluster, self.cal_probability(cluster, key), img_res=img_res))
        return clusters

    def __get_defect_id(self, defect_type):
        '''Return the length of the defect as its id.
        '''
        defect_id = {
            "Environment": 99,
            "No Defect Wall": 98,
            "Joint Defect": 4,
            "Crack": 2,
            "Loss of Section": 5,
            "Vegetation": 6,
            "Spalling": 3
        }
        return defect_id[defect_type]

    def cal_probability(self, cluster, defect_type) -> float:
        '''Calculate the probablity of a given cluster.

        Algorithm: average of all cells.

        No need to call seperately if using process().
        '''
        cell_ids = []
        for cell in cluster:
            cell_ids.append(self.convert_position_to_id(cell))

        prob = self.defects_raw.loc[self.defects_raw[self.cell_number_col].isin(
            cell_ids), defect_type].mean()
        return prob

    def save_clusters_to_csv(self, clusters: list[DefectCluster], filename="test.csv"):
        '''Save the DefectClusters into csv.

        columns = ["Cluster_Id", "Coordinates", "Probability", "Defect_Type"]

        No need to call seperately if using process().
        '''
        columns = ["Cluster_Id", "Coordinates", "Probability", "Defect_Type"]
        df = pd.DataFrame(columns=columns)

        row_in = 0
        for dc in clusters:
            df.loc[row_in] = [row_in, dc.get_region_coordinates(),
                              dc.probability, dc.type]
            row_in += 1

        self.__save_df_to_folder(df, filename, "processed defects")

    def __process_file(self, filename: str, verbose=False):
        '''Apply post processing and save the result.
        '''
        if verbose:
            print("Processing:", filename)

        self.load_defects(filename)
        if verbose:
            print("- data loaded.")

        self.filter_defects()
        if verbose:
            print("- filter applied.")

        defects_cluster = self.cal_defect_cluster(
            self.__get_offset(filename))
        if verbose:
            print("- defect region generated.")

        self.save_clusters_to_csv(
            defects_cluster, "".join([filename[:-4], "_processed", filename[-4:]]))
        if verbose:
            print("- result saved.")

    def process(self, path: str, verbose=False):
        '''Apply post processing to the given path/file and save the result(s).
        '''
        if os.path.isdir(path):
            for file in os.listdir(path):
                if fnmatch.fnmatch(file, "*.csv"):
                    self.__process_file(path+"/"+file, verbose)
        elif os.path.isfile(path):
            self.__process_file(path, verbose)
        else:
            print("Invalid path or filename.")

        print("Post process is completed for " + path)

    def __load_offset_config(self, image_info_path):
        return pd.read_csv(image_info_path)

    def __get_offset(self, filename):
        basename = os.path.basename(filename)
        conf = self.offset_config[self.offset_config["image_name"]
                                  == basename[:-4]]
        if conf.shape[0] == 0:
            return (0, 0)
        else:
            return (conf.iloc[0, 1], conf.iloc[0, 2])

    def add_coord_to_raw_data(self, path: str):
        '''Add coordinate for each defect and save the result to *_raw.csv.'''
        if os.path.isdir(path):
            for file in os.listdir(path):
                if fnmatch.fnmatch(file, "*.csv"):
                    filename = path+"/"+file
                    self.load_defects(filename)
                    img_res = self.__get_offset(file)
                    self.defects_raw["Coordinates"] = self.defects_raw.apply(
                        lambda row: self.__cal_coord(row, img_res), axis=1)
                    self.__save_df_to_folder(
                        self.defects_raw, filename[:-4] + "_raw" + filename[-4:], "raw defects with coordinates")

        else:
            print("Invalid path or filename.")

    def __cal_coord(self, row, img_res):
        '''Help function for pd.apply. img_res is used to calcuate the offset.'''
        cell = self.convert_id_to_position(row[self.cell_number_col])
        return DefectCluster(1, [cell], 0, img_res=img_res).get_region_coordinates()

    def __save_df_to_folder(self, df: pd.DataFrame, orignal_path, subfolder):
        # Create the folder if not exists
        # Note. It's the path of the datafile, i.e., is the "demo" folder not the root where the script runs
        actual_folder_path = os.path.dirname(orignal_path) + "/" + subfolder
        if not os.path.exists(actual_folder_path):
            os.makedirs(actual_folder_path)

        # Create full path to save the file
        if os.path.isfile(orignal_path):
            full_path = subfolder + "/" + orignal_path
        else:
            full_path = os.path.dirname(
                orignal_path) + "/" + subfolder + "/" + os.path.basename(orignal_path)

        df.to_csv(full_path, index=False)


if __name__ == '__main__':
    processor = PostProcessor("../demo/image_info.csv", shape=(22, 44))

    # process a single file
    processor.process("../demo/predictions/vt001.csv")

    # Process a folder
    processor.process("../demo/predictions")

    # Calculate the coordinates for raw defect in the folder
    processor.add_coord_to_raw_data("../demo/predictions")
