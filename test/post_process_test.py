from post_processing.post_process import *
import pandas as pd
from ast import literal_eval


file = "test/data/autotest.csv"
default_filter = {
    "Environment": 100,
    "No Defect Wall": 100,
    "Joint Defect": 100,
    "Crack": 90,
    "Loss of Section": 100,
    "Vegetation": 100,
    "Spalling": 90}
processor = PostProcessor("test/image_info.csv")


def test_get_cluster_corners():
    cells = [(2, 2), (1, 2), (1, 3), (1, 1)]
    result = [(1, 1), (1, 3), (2, 1), (2, 3)]
    assert DefectCluster(1, cells, 100).get_corner_cells() == result


def test_get_cluster_coordinates():
    cells = [(2, 2), (1, 2), (1, 3), (1, 1)]
    result = [(224, 224), (896, 224), (896, 672), (224, 672)]
    actual = DefectCluster(1, cells, 100).get_region_coordinates()
    assert actual == result


def test_get_cluster_coordinates_single():
    cells = [(0, 0), (0, 0), (0, 0), (0, 0)]
    result = [(0, 0), (224, 0), (224, 224), (0, 224)]
    assert DefectCluster(1, cells, 100).get_region_coordinates() == result


def test_PostProcessor():
    assert processor.shape == (45, 90)


def test_load_defects():
    processor.load_defects(file)

    assert processor.defects_raw.shape[0] == 4050


def test_defect_filter():
    processor.load_defects(file)
    processor.defect_filter = default_filter
    processor.filter_defects()

    assert processor.defects_filtered.shape[0] == 8
    assert processor.defects_filtered.values[7, 1] == 0.09
    assert processor.defects_filtered.values[7, 0] == 3322  # id


def test_convert_id_to_position():
    assert processor.convert_id_to_position(90) == (0, 89)
    assert processor.convert_id_to_position(1) == (0, 0)
    assert processor.convert_id_to_position(91) == (1, 0)
    assert processor.convert_id_to_position(272) == (3, 1)


def test_convert_position_to_id():
    assert processor.convert_position_to_id((0, 0)) == 1
    assert processor.convert_position_to_id((1, 0)) == 91


def test_get_defect_cluster_empty():
    processor.load_defects(file)
    processor.defect_filter = default_filter
    processor.filter_defects()

    assert processor.get_defect_cluster_cells(
        "Environment") == []


def test_get_defect_cluster_single():
    processor.load_defects(file)
    processor.defect_filter = default_filter
    processor.filter_defects()

    assert processor.get_defect_cluster_cells(
        "Crack") == [{(0, 0), (0, 1), (0, 2), (1, 0)}]


def test_get_defect_cluster_multi():
    processor.load_defects(file)
    processor.defect_filter = default_filter
    processor.filter_defects()

    assert processor.get_defect_cluster_cells(
        "Spalling") == [{(3, 1), (3, 2)}, {(28, 86)}, {(36, 81)}]


def test_get_adjacent_cells_centre(pos=(2, 2), shape=(4, 4)):
    result = processor.get_adjacent_cells(pos, shape)
    assert result == [(2, 1), (1, 2), (3, 2), (2, 3)]


def test_get_adjacent_cells_corner(shape=(4, 4)):
    pos = (0, 0)
    result = processor.get_adjacent_cells(pos, shape)
    assert result == [(1, 0), (0, 1)]

    pos = (0, 3)
    result = processor.get_adjacent_cells(pos, shape)
    assert result == [(0, 2), (1, 3)]

    pos = (3, 0)
    result = processor.get_adjacent_cells(pos, shape)
    assert result == [(2, 0), (3, 1)]

    pos = (3, 3)
    result = processor.get_adjacent_cells(pos, shape)
    assert result == [(3, 2), (2, 3)]


def test_cal_defect_cluster():
    processor.load_defects(file)
    processor.defect_filter = default_filter
    processor.filter_defects()

    clusters = processor.cal_defect_cluster((226, 228))
    assert len(clusters) == 4
    for c in clusters:
        if c.type == 5:
            assert len(c.cells) == 4
            assert c.probability == 99
            assert c.get_region_coordinates() == [(
                1, 2), (1, 674), (449, 2), (449, 674)]


def test_cal_prob():
    processor.load_defects(file)

    assert processor.cal_probability([(0, 0), (0, 1)], "Joint Defect") == 0.01


def test_save_clusters_to_csv():
    processor.load_defects(file)
    processor.defect_filter = default_filter
    processor.filter_defects()

    clusters = processor.cal_defect_cluster((226, 228))
    testfile = "test/data/autotest_clusters.csv"
    processor.save_clusters_to_csv(clusters, testfile)

    df = pd.read_csv("test/data/processed defects/autotest_clusters.csv")

    assert df.shape == (4, 4)
    assert df.iloc[2, 3] == 3
    assert literal_eval(df.iloc[0, 1]) == [
        (1, 2), (673, 2), (673, 450), (1, 450)]
