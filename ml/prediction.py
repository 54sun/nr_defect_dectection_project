import tensorflow as tf
import os
import numpy as np
import pandas as pd
from PIL import Image

# The prediction is adapted from the MS Custom Vision tutorial:
# https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/export-model-python


def defect_predict(bridge_image_path, sliced_image_folder, result_folder, model_file, label_file, hide_tf_log=True):
    '''Process predictions and save the results into the "predictions" folder.

    Parameters:
    image_folder: The folder/path contains the source bridge images.
    sliced_image_folder: The name of the folder for sliced images, which will be created under the image_folder.
    result_folder: The subfolder that saves the prediction results.
    '''

    if hide_tf_log:
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

    # Create the folder to save the predctions result
    prediction_result_path = os.path.join(bridge_image_path, result_folder)
    os.makedirs(prediction_result_path, exist_ok=True)

    # Import the TF graph
    graph_def = tf.compat.v1.GraphDef()
    with tf.io.gfile.GFile(model_file, 'rb') as f:
        graph_def.ParseFromString(f.read())
        tf.import_graph_def(graph_def, name='')

    # Load the list of labels.
    labels = []
    with open(label_file, 'rt') as lf:
        for l in lf:
            labels.append(l.strip())

    # These names are part of the model and cannot be changed.
    output_layer = 'loss:0'
    input_node = 'Placeholder:0'

    with tf.compat.v1.Session() as sess:
        try:
            prob_tensor = sess.graph.get_tensor_by_name(output_layer)

            # Each sub_folder represents one image
            for sub_folder in os.listdir(os.path.join(bridge_image_path, sliced_image_folder)):
                print(" Predicting: " + sub_folder)
                prediction = []
                for img_file in os.listdir(os.path.join(bridge_image_path, sliced_image_folder, sub_folder)):
                    # Load sliced images
                    sliced_image = Image.open(os.path.join(
                        bridge_image_path, sliced_image_folder, sub_folder, img_file))

                    # Convert to OpenCV format
                    img = __convert_to_opencv(sliced_image)

                    # Run prediction
                    pred = sess.run(prob_tensor, {input_node: [img]})

                    # Prepare the result
                    # result = sub_image_id + probablity per label + predicted_defect
                    # img_file format: [image name]_[sub_image_id][.jpg]
                    result = [img_file[(img_file.rfind("_")+1):-4]]
                    # Predicted defect is the one with higested probability
                    result[1:1] = [labels[np.argmax(pred)]]
                    # Append the probabilities
                    result[1:1] = pred[0]*100
                    # Add current result into the prediction
                    prediction.append(result)

                # Save the result as csv
                df = pd.DataFrame(prediction, columns=[
                                  "Sub_image ID"] + labels + ["Predicted Defect"])
                df.to_csv(os.path.join(
                    prediction_result_path, sub_folder+".csv"), index=False, float_format="%.2f")

        except KeyError:
            print("Couldn't find classification output layer: " + output_layer + ".")
            print("Verify this a model exported from an Object Detection project.")
            exit(-1)


def __convert_to_opencv(image):
    # RGB -> BGR conversion is performed as well.
    r, g, b = np.array(image).T
    opencv_image = np.array([b, g, r]).transpose()
    return opencv_image


if __name__ == '__main__':
    import time

    tic = time.perf_counter()

    print("\nPrediction started ..... \n")
    # run prediction
    defect_predict(
        "../demo", "sliced", "predictions", "../demo/model/model.pb", "../demo/model/labels.txt")

    # record time used in second
    toc = time.perf_counter()
    print("\nPrediction has completed in " + f"{toc - tic:0.4f} seconds")
