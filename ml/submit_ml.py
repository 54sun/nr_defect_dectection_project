import pandas as pd
import requests


def submit_ml_db(data_file,examination_dataID):
    # Submit defects to the DB

   
    data = pd.read_csv(data_file)
    
    df = pd.DataFrame(data, columns=['Cluster_Id', 'Coordinates', 'Probability', 'Defect_Type'])
        
    # print(df)
    
    for row in df.itertuples():
        cmiCode = 1
        
       # indices_Coordinates = [list(x) for x in row.Coordinates]
       # print(type(row.Coordinates))
        indices_Coordinates1 = row.Coordinates.replace("(", "[")
        indices_Coordinates2 = indices_Coordinates1.replace(")", "]")
       # print(indices_Coordinates2)
       
       ## todo - we need the api to delete the previous machine learning dataset
       
        url = 'https://examcoreapi20210628143308.azurewebsites.net/api/DefectMls'
        
        myobj = {"defectTypeId": row.Defect_Type,
                 "positionCoord": indices_Coordinates2,
                 "referenceNumber": row.Cluster_Id,
                 "percentageMatch": row.Probability,
                 "cmiCodeId": cmiCode,
                 "examinationDatasetId": examination_dataID
                 }

        x = requests.post(url, json=myobj)
        print(x.status_code)


    print('*******************')
    print(data_file + ' update completed! Thank you!')
