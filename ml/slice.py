import glob
import os
from PIL import Image
import pandas as pd


def slice_image(image_folder, sliced_image_folder, image_info_file, sub_image_width=224, sub_image_height=224):
    '''Slice all images in the given folder.

    Sliced images are saved in to the "sliced" folder, with subfolders for each image.

    If a dimension cannot be perfectly sliced, equal margin will be applied at both end,
    i.e., "100px sclied by 30px" will result in a 5px margin.

    Parameters:
    image_folder: The folder/path contains the source bridge images.
    sliced_image_folder: The name of the folder for sliced images, which will be created under the image_folder.
    image_info_file: The name of the image info csv.
    '''
    # Remove the pixel limit
    Image.MAX_IMAGE_PIXELS = None

    # To save image_name, width, height
    image_info = []

    # Process each 360-degree image of the given bridge
    for image_path in glob.glob(os.path.join(image_folder, '*.jpg')):
        # Initialize image folder
        try:
            print(" Slicing: " + str(image_path))
            # Get the name of the 360-degree image (ingoring the .jpg suffix)
            image_name = os.path.basename(image_path)
            image_name = os.path.splitext(image_name)[0]

            # Create folders for sliced images, which are saved to a subfolder named by the image.
            os.makedirs(os.path.join(image_folder,
                                     sliced_image_folder, image_name), exist_ok=True)

            # Read the 360-degree image
            img = Image.open(image_path)
            img_width, img_height = img.size

            # Save the image info
            image_info.append([image_name, img_width, img_height])

            # Compute start and end pixels for height
            start_height = int(img_height % sub_image_height / 2)
            end_height = img_height - start_height - 1

            # Compute start and end pixels for width
            start_width = int(img_width % sub_image_width / 2)
            end_width = img_width - start_width - 1

            # A counter for sub-images
            sub_image_num = 1
            # Slice 360-image: left->right, top->bottom
            # Rows iterate through height
            for row in range(start_height, end_height, sub_image_height):
                # Columns iterate through width
                for column in range(start_width, end_width, sub_image_width):
                    # Setting the points for cropped image
                    left = column
                    top = row
                    right = column + sub_image_width
                    bottom = row + sub_image_height
                    # Crop sub-image out of 360-degree image
                    crop = img.crop((left, top, right, bottom))
                    # Save sub-image
                    save_to = os.path.join(
                        image_folder, sliced_image_folder, image_name, image_name + "_{}.jpg")
                    # Save sub image using original DPI
                    crop.save(save_to.format(sub_image_num),
                              dpi=img.info["dpi"])
                    # Move to next sub-image
                    sub_image_num += 1

        except Exception as err:
            print(err)

    # Save the images info
    df = pd.DataFrame(image_info, columns=["image_name", "width", "height"])
    df.to_csv(os.path.join(image_folder,
                           image_info_file), index=False)


if __name__ == '__main__':
    slice_image("../demo", "sliced", "image_info.csv")
