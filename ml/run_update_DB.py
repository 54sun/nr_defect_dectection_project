import pandas as pd
import submit_ml


## this script is to update machine learning data table with csv processed files

# data_folder is for the all processed csv file 

data_folder ='C:/Users/con3wus/Desktop/Processed/'

# we need to have a update list for processed file name and examination dataset id

update_list ='Update_list.csv'

data = pd.read_csv(update_list)
df = pd.DataFrame(data, columns=['filename', 'ID'])
i=0
for row in df.itertuples():
    data_file = data_folder + row.filename
    examination_id = row.ID
    # submit_ml.submit_ml_db(data_file,examination_id)
    print (data_file + ' ' + str(examination_id))
    i+=1
    
print('submitted ' + str(i) +' files.')    

